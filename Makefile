.DEFAULT_GOAL := install

install:
	chmod +x ./charsel
	mkdir -p /usr/share/charsel/charfiles/
	cp charfiles/* /usr/share/charsel/charfiles/
	cp shell-completions /usr/share/charsel/
	mkdir /usr/share/doc/charsel
	cp README.md /usr/share/doc/charsel/
	cp ./charsel /usr/bin/

uninstall:
	rm -rf /usr/share/charsel/
	rm -rf /usr/share/doc/charsel/
	rm -rf /usr/bin/charsel

reinstall:
	make uninstall
	make install

purge:
	rm -rf  ${HOME}/.local/share/charsel/
	rm -rf ${HOME}/.cache/charsel/

install-completions-bash:
	echo "source /usr/share/charsel/shell-completions" >> '${HOME}/.bashrc'
	echo "autoload bashcompinit ; bashcompinit ; source /usr/share/charsel/shell-completions" >> '${ZDOTDIR}/.zshrc'
